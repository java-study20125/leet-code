package com.gaibu.code07Hash表;

import java.util.*;

public class _1002_查找常用字符 {
    public List<String> commonChars(String[] words) {
        List<String> result = new ArrayList<>();
        if (words.length < 1) {
            return result;
        }

        List<Map<Character, Integer>> list = new ArrayList<>();
        for (String word : words) {
            list.add(initCountMap(word));
        }

        Map<Character, Integer> characterIntegerMap = list.get(0);
        Set<Character> characters = characterIntegerMap.keySet();

        for (Character element : characters) {
            Integer count = null;
            boolean find = false;
            for (Map<Character, Integer> map : list) {
                Integer count2 = map.get(element);
                if (count2 == null) {
                    find = true;
                    break;
                }
                if (count == null || count2 < count) {
                    count = count2;
                }
            }

            if (!find) {
                for (Integer i = 0; i < count; i++) {
                    result.add(element.toString());
                }
            }
        }

        return result;

    }

    private Map<Character, Integer> initCountMap(String t) {
        char[] tCharArray = t.toCharArray();
        Map<Character, Integer> tCharCountMap = new TreeMap<>();
        for (char c : tCharArray) {
            Integer count = tCharCountMap.getOrDefault(c, 0);
            count++;
            tCharCountMap.put(c, count);
        }

        return tCharCountMap;
    }
}
