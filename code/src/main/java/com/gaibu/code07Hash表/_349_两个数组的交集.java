package com.gaibu.code07Hash表;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class _349_两个数组的交集 {

    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set1 = initSet(nums1);
        Set<Integer> set2 = initSet(nums2);
        List<Integer> result = new ArrayList<>();

        for (Integer num : set1) {
            if (set2.contains(num)) {
                result.add(num);
            }
        }
        int[] r = new int[result.size()];
        for (int i = 0; i < result.size(); i++) {
            r[i] = result.get(i);
        }

        return r;
    }

    private Set<Integer> initSet(int[] nums) {
        Set<Integer> set = new TreeSet<>();
        for (int num : nums) {
            set.add(num);
        }

        return set;
    }
}
