package com.gaibu.code07Hash表;

import java.util.Map;
import java.util.TreeMap;

public class _242_有效的字母异位词 {

    public boolean isAnagram(String s, String t) {
        Map<Character, Integer> sCharCountMap = initCountMap(s);
        Map<Character, Integer> tCharCountMap = initCountMap(t);

        return sCharCountMap.equals(tCharCountMap);
    }

    private Map<Character, Integer> initCountMap(String t) {
        char[] tCharArray = t.toCharArray();
        Map<Character, Integer> tCharCountMap = new TreeMap<>();
        for (char c : tCharArray) {
            Integer count = tCharCountMap.getOrDefault(c, 0);
            count++;
            tCharCountMap.put(c, count);
        }

        return tCharCountMap;
    }
}
