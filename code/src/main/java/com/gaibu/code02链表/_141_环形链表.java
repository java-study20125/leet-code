package com.gaibu.code02链表;

import java.util.HashSet;
import java.util.Set;

/**
 * <a href="https://leetcode.cn/problems/linked-list-cycle/description/">_141_环形链表</a>
 */
public class _141_环形链表 {
    public boolean hasCycle(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }

        ListNode slow = head;
        ListNode fast = head.next;

        while (fast != null && fast.next != null) {
            if (slow.equals(fast)) {
                return true;
            }

            slow = slow.next;
            fast = fast.next.next;
        }

        return false;
    }


    public boolean hasCycle2(ListNode head) {
        if (head == null || head.next == null) {
            return false;
        }
        Set<ListNode> distinct = new HashSet<>();
        ListNode curr = head;
        while (curr.next != null) {
            if (!distinct.add(curr)) {
                return true;
            }
            curr = curr.next;
        }
        return false;
    }
}
