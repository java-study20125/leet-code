package com.gaibu.code02链表;

/**
 * <a href="https://leetcode.cn/problems/delete-node-in-a-linked-list/description/">237. 删除链表中的节点</a>
 */
public class _237_删除链表中的节点 {

    public void deleteNode(ListNode node) {
        node.val = node.next.val;
        node.next = node.next.next;
    }

}
