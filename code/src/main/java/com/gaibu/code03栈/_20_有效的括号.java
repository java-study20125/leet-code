package com.gaibu.code03栈;

import java.util.*;

/**
 * <a href="https://leetcode.cn/problems/valid-parentheses/description/">_20_有效的括号</a>
 */
public class _20_有效的括号 {

    public static void main(String[] args) {
        _20_有效的括号 t = new _20_有效的括号();
        System.out.println(t.isValid("1(){}"));
    }

    public boolean isValid(String s) {
        if (s == null || s.isBlank() || s.length() == 1) {
            return false;
        }
        Stack<Character> stack = new Stack<>();
        char[] charArray = s.toCharArray();
        Map<Character, Character> hitMap = new HashMap<>();
        hitMap.put('(', ')');
        hitMap.put('{', '}');
        hitMap.put('[', ']');

        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];

            Set<Character> left = hitMap.keySet();
            if (left.contains(c)) {
                stack.push(c);
            } else {
                if (stack.isEmpty()) {
                    return false;
                }
                Character l = stack.pop();
                if (!Objects.equals(hitMap.get(l), c)) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    public boolean isValid2(String s) {
        String[] hits = {"[]", "{}", "()"};
        while (s.contains(hits[0]) || s.contains(hits[1]) || s.contains(hits[2])) {
            for (int i = 0; i < hits.length; i++) {
                s = s.replace(hits[i], "");
            }
        }
        return s.isEmpty();
    }
}
