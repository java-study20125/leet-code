package com.gaibu.code01;

/**
 * <a href="https://leetcode.cn/problems/longest-common-prefix/description/">_14_最长公共前缀</a>
 */
public class _14_最长公共前缀 {

    public static void main(String[] args) {
        _14_最长公共前缀 t = new _14_最长公共前缀();
        System.out.println(t.longestCommonPrefix(new String[]{"dog","racecar","car"}));
    }

    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        }

        if (strs.length == 1) {
            return strs[0];
        }

        StringBuilder prefix = new StringBuilder("");
        char[] firstCharArr = strs[0].toCharArray();
        for (int i = 1; i < strs.length; i++) {
            prefix = new StringBuilder("");
            char[] nextCharArray = strs[i].toCharArray();
            for (int j = 0; j < firstCharArr.length; j++) {
                if (nextCharArray.length > j && firstCharArr[j] == nextCharArray[j]) {
                    prefix.append(firstCharArr[j]);
                } else {
                    if (j == 0) {
                        return "";
                    } else {
                        firstCharArr = prefix.toString().toCharArray();
                    }
                    break;
                }
            }

        }
        return prefix.toString();
    }
}
