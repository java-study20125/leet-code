package com.gaibu.code01;

import java.util.HashMap;
import java.util.Map;

/**
 * <a href="https://leetcode.cn/problems/longest-palindrome/description/">_409_最长回文串</a>
 */
public class _409_最长回文串 {

    public static void main(String[] args) {
        _409_最长回文串 t = new _409_最长回文串();
        System.out.println(t.longestPalindrome("abccccdd"));
    }

    public int longestPalindrome(String s) {
        if (s.length() == 1) {
            return 1;
        }

        char[] charArray = s.toCharArray();
        Map<Character, Integer> charCountMap = new HashMap<>();
        for (char c : charArray) {
            Integer count = charCountMap.getOrDefault(c, 0);
            charCountMap.put(c, count + 1);
        }

        int length = 0;
        int jishu = 0;
        for (Map.Entry<Character, Integer> entry : charCountMap.entrySet()) {
            Integer value = entry.getValue();

            // 长度为偶数
            if (value % 2 == 0) {
                length = length + value;
            }
            // 长度为奇数
            else {
                jishu++;
                if (value - 1 > 0) {
                    length = length + value - 1;
                }
            }

        }

        if (jishu > 0) {
            return length + 1;
        } else {
            return length;
        }
    }
}
