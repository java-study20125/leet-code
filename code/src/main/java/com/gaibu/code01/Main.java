package com.gaibu.code01;

import cn.hutool.core.lang.Assert;

/**
 * 斐波那契数列
 */
public class Main {

    public static void main(String[] args) {
        int[] nums = {1,1,2,3,5,8,13,21,34,55,89};
        // 1,1,2,3,5,8,13,21,34,55,89
        // 3 1
        // 4 2
        // 5 3
        for (int i = 1; i <= nums.length; i++) {
            Assert.equals(fb(i), nums[i-1]);
        }
    }

    public static int fb(int n) {
        if (n <= 2) {
            return 1;
        }

        int first = 1;
        int second = 1;

        for (int i = 0; i < n - 2; i++) {
            int sum = first + second;
            first = second;
            second = sum;
        }

        return second;
    }

    public static int fb2(int n) {
        if (n <= 2) {
            return 1;
        }

        return fb(n - 1) + fb(n - 2);
    }
}
