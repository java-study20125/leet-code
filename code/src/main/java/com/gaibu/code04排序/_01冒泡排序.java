package com.gaibu.code04排序;

import cn.hutool.core.collection.CollUtil;

import java.util.Arrays;

public class _01冒泡排序 {

    public static void main(String[] args) {
        int[] arr = new int[]{2, 4, 2, 3, 4, 5, 6, 1, 1};

        printArray(bubbleSort2(arr));
        printArray(_02_选择排序.selectSort(Arrays.copyOf(arr, arr.length)));
        printArray(_03堆排序.heapSort(Arrays.copyOf(arr, arr.length)));
    }

    /**
     * 冒泡排序
     */
    public static int[] bubbleSort(int[] args) {
        if (args.length <= 1) {
            return args;
        }

        for (int i = args.length - 1; i > 0; i--) {
            for (int start = 1; start <= i; start++) {
                if (args[start] < args[start - 1]) {
                    int tmp = args[start];
                    args[start] = args[start - 1];
                    args[start - 1] = tmp;
                }
            }
        }

        return args;
    }

    public static int[] bubbleSort2(int[] args) {
        if (args.length <= 1) {
            return args;
        }

        for (int i = args.length - 1; i > 0; i--) {
            int sortIndex = 0;
            for (int start = 1; start <= i; start++) {
                if (args[start] < args[start - 1]) {
                    int tmp = args[start];
                    args[start] = args[start - 1];
                    args[start - 1] = tmp;

                    sortIndex = start;
                }
            }

            i = sortIndex;
        }

        return args;
    }

    private static void printArray(int[] args) {
        String join = CollUtil.join(CollUtil.newArrayList(args), " ");

        System.out.println(join);
    }
}
