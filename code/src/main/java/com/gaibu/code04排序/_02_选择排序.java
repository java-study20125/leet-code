package com.gaibu.code04排序;

public class _02_选择排序 {

    /**
     * 选择排序
     */
    public static int[] selectSort(int[] args) {
        if (args.length <= 1) {
            return args;
        }

        for (int end = args.length-1; end >0 ; end--) {
            int maxIndex = 0;
            for (int start = 0; start <= end; start++) {
                if (args[start] > args[maxIndex]){
                    maxIndex = start;
                }
            }

            int tmp = args[end];
            args[end] = args[maxIndex];
            args[maxIndex] = tmp;
        }
        return args;
    }
}
