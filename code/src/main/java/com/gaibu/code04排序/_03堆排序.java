package com.gaibu.code04排序;

import java.util.PriorityQueue;

public class _03堆排序 {

    public static int[] heapSort(int[] args) {
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        for (int arg : args) {
            heap.add(arg);
        }

        for (int i = 0; i < args.length; i++) {
            Integer max = heap.poll();
            args[i] = max;
        }
        return args;
    }

}
