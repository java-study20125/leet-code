package com.gaibu.code05字符串;

import java.util.HashSet;
import java.util.Set;

public class _409_最长回文串 {
    public int longestPalindrome(String s) {
        if (s == null || s.isEmpty()) {
            return -1;
        }
        if (s.length() == 1) {
            return 1;
        }
        Set<Character> set = new HashSet<>();

        char[] charArray = s.toCharArray();
        int findNum = 0;
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[i];
            if (!set.contains(c)) {
                set.add(c);
            } else {
                findNum++;
                set.remove(c);
            }
        }

        if (!set.isEmpty()) {
            return findNum * 2 + 1;
        } else {
            return findNum * 2;
        }
    }
}
