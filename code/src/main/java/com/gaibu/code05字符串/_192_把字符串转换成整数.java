package com.gaibu.code05字符串;

public class _192_把字符串转换成整数 {

    public static void main(String[] args) {
        System.out.println(new _192_把字符串转换成整数().myAtoi("words and 987"));
    }

    public int myAtoi(String str) {
        if (str == null) {
            return 0;
        }

        str = str.trim();

        boolean zfFlag = true;

        if (str.startsWith("-")) {
            str = str.substring(1);
            zfFlag = false;
        } else if (str.startsWith("+")) {
            str = str.substring(1);
        }

        char[] charArray = str.toCharArray();
        int result = 0;

        if (!Character.isDigit(charArray[0])){
            return 0;
        }
        int xi = 0;
        for (int i = 0; i < charArray.length; i++) {
            char c = charArray[charArray.length - i - 1];
            if (Character.isDigit(c)) {
                Integer kk = Integer.valueOf(c + "");
                Integer xishu = (int) Math.pow(10,xi);
                xi++;
                result = result + kk * xishu;
            }
        }
        return zfFlag ? result : -1 * result;
    }
}
