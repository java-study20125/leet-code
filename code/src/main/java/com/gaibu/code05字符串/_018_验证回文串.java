package com.gaibu.code05字符串;

public class _018_验证回文串 {

    public boolean isPalindrome2(String s) {
        if (s == null || "".equals(s)) {
            return true;
        }

        if (s.length() == 1) {
            return true;
        }

        char[] charArray = s.toCharArray();


        int left = 0;
        int right = charArray.length - 1;
        while (left < right) {
            while (left < right && !Character.isLetterOrDigit(s.charAt(left))) {
                left++;
            }
            while (left < right && !Character.isLetterOrDigit(s.charAt(right))) {
                right--;
            }

            if (Character.toLowerCase(charArray[left]) != Character.toLowerCase(charArray[right])) {
                return false;
            }

            left++;
            right--;
        }
        return true;
    }
}
