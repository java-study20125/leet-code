package com.gaibu.code05字符串;


import java.util.Arrays;

public class _14最长公共前缀 {
    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        }
        if (strs.length == 1) {
            return strs[0];
        }
        Arrays.sort(strs);

        String leftStr = strs[0];
        String rightStr = strs[strs.length - 1];
        if ("".equals(leftStr) || "".equals(rightStr)) {
            return "";
        }

        char[] charArray = leftStr.toCharArray();
        int loopSize = Math.min(leftStr.length(), rightStr.length());
        StringBuilder builder = new StringBuilder("");
        for (int i = 0; i < loopSize; i++) {
            if (charArray[i] == rightStr.charAt(i)) {
                builder.append(charArray[i]);
            } else {
                break;
            }
        }

        return builder.toString();

    }
}
