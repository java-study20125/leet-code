package com.gaibu.code05字符串;

public class _344_反转字符串 {

    public static void main(String[] args) {
        char[] s =  new char[]{'a','b','c'};
        new _344_反转字符串().reverseString(s);

        System.out.println(s);
    }

    public void reverseString(char[] s) {
        int left = 0;
        int right = s.length - 1;
        int loop = s.length/2;

        while (loop-->0){
            char leftChar = s[left];
            s[left] = s[right];
            s[right] = leftChar;

            left++;
            right--;
        }
    }
}
