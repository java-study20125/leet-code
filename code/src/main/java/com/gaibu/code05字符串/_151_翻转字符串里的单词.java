package com.gaibu.code05字符串;

public class _151_翻转字符串里的单词 {

    public static void main(String[] args) {
        System.out.println(new _151_翻转字符串里的单词().reverseWords("a good  example"));
    }

    public String reverseWords(String s) {
        // 移除前后空格
        String trim = s.trim();

        char[] charArray = trim.toCharArray();

        // 整体字符串反转
        reverse(charArray, 0, charArray.length - 1);

        int start;
        int end = 0;

        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == ' ') {
                start = end;
                end = i - 1;

                reverse(charArray, start, end);
                // 跳过空格
                int skip = 1;
                while (charArray[i + 1] == ' ') {
                    skip++;
                    i++;
                }
                end = end + skip + 1;
            }
        }

        // 反转最后一个单词
        reverse(charArray, end, charArray.length - 1);

        // 移除中间多余的空格
        String result = new String();
        for (int i = 0; i < charArray.length; i++) {
            result = result + charArray[i];
            if (charArray[i] == ' ') {
                // 跳过空格
                while (charArray[i + 1] == ' ') {
                    i++;
                }
            }
        }
        return result;
    }

    public void reverse(char[] charArray, int left, int right) {
        while (left < right) {
            char tmp = charArray[left];
            charArray[left] = charArray[right];
            charArray[right] = tmp;

            left++;
            right--;
        }
    }
}
