package com.gaibu.code06数组;

public class _59_螺旋矩阵II {

    public static void main(String[] args) {
        print(new _59_螺旋矩阵II().generateMatrix(3));
    }

    private static void print(int[][] nums) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                System.out.print(nums[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int[][] generateMatrix(int n) {
        int[][] result = new int[n][n];
        int count = 1;
        int loop = n / 2;
        int startX = 0;
        int startY = 0;
        int offset = 1;

        int i, j;
        while (loop > 0) {
            i = startX;
            j = startY;

            // 下面开始的四个for就是模拟转了一圈
            // 模拟填充上行从左到右(左闭右开)
            for (; i < n - offset; i++) {
                result[j][i] = count++;
            }

            // 模拟填充右列从上到下(左闭右开)
            for (; j < n - offset; j++) {
                result[j][i] = count++;
            }

            // 模拟填充下行从右到左(左闭右开)
            for (; i > startX; i--) {
                result[j][i] = count++;
            }

            // 模拟填充左列从下到上(左闭右开)
            for (; j > startY; j--) {
                result[j][i] = count++;
            }

            // 第二圈开始的时候，起始位置要各自加1， 例如：第一圈起始位置是(0, 0)，第二圈起始位置是(1, 1)
            startX++;
            startY++;
            offset++;
            loop--;
        }

        // 如果n为奇数的话，需要单独给矩阵最中间的位置赋值
        if (n % 2 == 1) {
            result[n / 2][n / 2] = count;
        }

        return result;
    }
}
