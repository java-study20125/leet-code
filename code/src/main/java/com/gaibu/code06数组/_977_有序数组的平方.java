package com.gaibu.code06数组;

public class _977_有序数组的平方 {
    public static void main(String[] args) {
        int[] nums =new int []{-4,-1,0,3,10};
        System.out.println(new _977_有序数组的平方().sortedSquares(nums));
    }

    public int[] sortedSquares(int[] nums) {
        int[] result = new int[nums.length];

        int left = 0;
        int right = nums.length - 1;
        int index = nums.length - 1;
        while (left <= right) {
            int numLeft = nums[left] * nums[left];
            int numRight = nums[right] * nums[right];
            if (numLeft <= numRight) {
                right--;
                result[index] = numRight;
                index--;
            }else {
                left++;
                result[index] = numLeft;
                index--;
            }
        }
        return result;
    }
}
